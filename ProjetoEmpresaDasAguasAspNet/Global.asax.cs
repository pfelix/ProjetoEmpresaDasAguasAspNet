﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjetoEmpresaDasAguasAspNet.Helpers;
using ProjetoEmpresaDasAguasAspNet.Models;

namespace ProjetoEmpresaDasAguasAspNet
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // Inicializar BD e fazer migrações 
            // Context: ProjetoEmpresaDasAguasAspNetContext
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ProjetoEmpresaDasAguasAspNet.Models.ProjetoEmpresaDasAguasAspNetContext, Migrations.Configuration>());
            // Context: ProjetoEmpresaDasAguasAspNetContext
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ProjetoEmpresaDasAguasAspNet.Models.ApplicationDbContext, Migrations.ApplicationDbContext.Configuration>());

            // Usar Data Context criado pelo asp.net para o processo de autenticação
            ApplicationDbContext db = new ApplicationDbContext();
            // Criar Roles
            CreateRoles(db);
            // Criar SuperUser
            CreateSuperUser(db);
            // Adicionar Roles ao SuperUser
            AddPermissionsToSuperUser(db);
            db.Dispose();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        /// <summary>
        /// Metodo para adicionar Roles ao SuperUser
        /// </summary>
        /// <param name="db"></param>
        private void AddPermissionsToSuperUser(ApplicationDbContext db)
        {
            // Objeto para manipular user
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var user = userManager.FindByName("sa@projetoempresadasaguas.pt");

            AddsHelper.AdicionarRoles(user.Id, userManager);
        }

        /// <summary>
        /// Metodo para criar o SuperUser
        /// </summary>
        /// <param name="db"></param>
        private void CreateSuperUser(ApplicationDbContext db)
        {
            // Objeto para manipular os users
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var user = userManager.FindByName("sa");

            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = "sa@projetoempresadasaguas.pt",
                    Email = "sa@projetoempresadasaguas.pt",
                    TipoConta = "Administrador"
                };

                userManager.Create(user,"Senhac30!");
            }
        }

        /// <summary>
        /// Metodo para criar as ROLES
        /// </summary>
        /// <param name="db"></param>
        private void CreateRoles(ApplicationDbContext db)
        {
            // Objeto para manipular as Roles
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            // Roles Admin
            if (!roleManager.RoleExists("Admin"))
            {
                roleManager.Create(new IdentityRole("Admin"));
            }

            // Roles Clientes
            if (!roleManager.RoleExists("Clientes"))
            {
                roleManager.Create(new IdentityRole("Clientes"));
            }

            if (!roleManager.RoleExists("Clientes View"))
            {
                roleManager.Create(new IdentityRole("Clientes View"));
            }

            if (!roleManager.RoleExists("Clientes Create"))
            {
                roleManager.Create(new IdentityRole("Clientes Create"));
            }

            if (!roleManager.RoleExists("Clientes Edit"))
            {
                roleManager.Create(new IdentityRole("Clientes Edit"));
            }

            if (!roleManager.RoleExists("Clientes Delete"))
            {
                roleManager.Create(new IdentityRole("Clientes Delete"));
            }

            // Roles Consumos
            if (!roleManager.RoleExists("Consumos"))
            {
                roleManager.Create(new IdentityRole("Consumos"));
            }

            if (!roleManager.RoleExists("Consumos View"))
            {
                roleManager.Create(new IdentityRole("Consumos View"));
            }

            if (!roleManager.RoleExists("Consumos Create"))
            {
                roleManager.Create(new IdentityRole("Consumos Create"));
            }

            if (!roleManager.RoleExists("Consumos Edit"))
            {
                roleManager.Create(new IdentityRole("Consumos Edit"));
            }

            if (!roleManager.RoleExists("Consumos Delete"))
            {
                roleManager.Create(new IdentityRole("Consumos Delete"));
            }

            // Roles Escaloes
            if (!roleManager.RoleExists("Escaloes"))
            {
                roleManager.Create(new IdentityRole("Escaloes"));
            }

            if (!roleManager.RoleExists("Escaloes View"))
            {
                roleManager.Create(new IdentityRole("Escaloes View"));
            }

            if (!roleManager.RoleExists("Escaloes Create"))
            {
                roleManager.Create(new IdentityRole("Escaloes Create"));
            }

            if (!roleManager.RoleExists("Escaloes Edit"))
            {
                roleManager.Create(new IdentityRole("Escaloes Edit"));
            }

            if (!roleManager.RoleExists("Escaloes Delete"))
            {
                roleManager.Create(new IdentityRole("Escaloes Delete"));
            }

            // Roles Faturas
            if (!roleManager.RoleExists("Faturas"))
            {
                roleManager.Create(new IdentityRole("Faturas"));
            }

            if (!roleManager.RoleExists("Faturas View"))
            {
                roleManager.Create(new IdentityRole("Faturas View"));
            }

            if (!roleManager.RoleExists("Faturas Create"))
            {
                roleManager.Create(new IdentityRole("Faturas Create"));
            }

            // Role Users
            if (!roleManager.RoleExists("Users"))
            {
                roleManager.Create(new IdentityRole("Users"));
            }

            if (!roleManager.RoleExists("Users View"))
            {
                roleManager.Create(new IdentityRole("Users View"));
            }

            if (!roleManager.RoleExists("Users Create"))
            {
                roleManager.Create(new IdentityRole("Users Create"));
            }

            if (!roleManager.RoleExists("Users Edit"))
            {
                roleManager.Create(new IdentityRole("Users Edit"));
            }

            if (!roleManager.RoleExists("Users Delete"))
            {
                roleManager.Create(new IdentityRole("Users Delete"));
            }
        }
    }
}
