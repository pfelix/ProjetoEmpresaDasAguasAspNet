﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjetoEmpresaDasAguasAspNet.Models;
using ProjetoEmpresaDasAguasAspNet.ViewModels;

namespace ProjetoEmpresaDasAguasAspNet.Controllers
{
    [Authorize(Roles = "Users")]
    public class UsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Users
        [Authorize(Roles = "Users View")]
        public ActionResult Index()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            // Lista de Users existentes
            var users = userManager.Users.ToList();

            // Lista do tipo UserView
            var usersView = new List<UserViewModel>();

            // Passar para a View
            foreach (var user in users)
            {
                var userView = new UserViewModel
                {
                    Email = user.Email,
                    Name = user.UserName,
                    UserId = user.Id,
                    TipoConta = user.TipoConta
                    
                };

                usersView.Add(userView);
            }

            return View(usersView);
        }

        //// GET: Users/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        //// GET: Users/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Users/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Users/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: Users/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: Users/Delete/5
        [Authorize(Roles = "Users Delete")]
        public ActionResult Delete(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var user = userManager.Users.ToList().Find(u => u.Id == userId);

            if (user == null)
            {
                return HttpNotFound();
            }

            var userView = new UserViewModel
            {
                Email = user.Email,
                Name = user.UserName,
                UserId = user.Id,
                TipoConta = user.TipoConta
            };

            return View(userView);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string userId)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var user = userManager.Users.ToList().Find(u => u.Id == userId);

            userManager.Delete(user);

            // Lista de Users existentes
            var users = userManager.Users.ToList();

            // Lista do tipo UserView
            var usersView = new List<UserViewModel>();

            // Passar para a View
            foreach (var user1 in users)
            {
                var userView = new UserViewModel
                {
                    Email = user1.Email,
                    Name = user1.UserName,
                    UserId = user1.Id,
                    TipoConta = user.TipoConta
                };

                usersView.Add(userView);
            }

            return View("Index", usersView);

        }
    }
}
