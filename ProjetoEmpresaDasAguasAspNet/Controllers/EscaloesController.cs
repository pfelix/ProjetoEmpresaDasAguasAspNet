﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjetoEmpresaDasAguasAspNet.Models;

namespace ProjetoEmpresaDasAguasAspNet.Controllers
{
    [Authorize(Roles = "Escaloes")]
    public class EscaloesController : Controller
    {
        private ProjetoEmpresaDasAguasAspNetContext db = new ProjetoEmpresaDasAguasAspNetContext();

        // GET: Escaloes
        [Authorize(Roles = "Escaloes View")]
        public ActionResult Index()
        {
            var listaEscaloes = db.Escaloes.OrderBy(e => e.Minimo).ToList();

            return View(listaEscaloes);
        }

        // GET: Escaloes/Create
        [Authorize(Roles = "Escaloes Create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Escaloes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdEscalao,Minimo,ValorUnitario")] Escalao escalao)
        {
            if (ModelState.IsValid)
            {
                db.Escaloes.Add(escalao);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(escalao);
        }

        // GET: Escaloes/Edit/5
        [Authorize(Roles = "Escaloes Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Escalao escalao = db.Escaloes.Find(id);
            if (escalao == null)
            {
                return HttpNotFound();
            }
            return View(escalao);
        }

        // POST: Escaloes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdEscalao,Minimo,ValorUnitario")] Escalao escalao)
        {
            if (ModelState.IsValid)
            {
                db.Entry(escalao).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(escalao);
        }

        // GET: Escaloes/Delete/5
        [Authorize(Roles = "Escaloes Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Escalao escalao = db.Escaloes.Find(id);
            if (escalao == null)
            {
                return HttpNotFound();
            }
            return View(escalao);
        }

        // POST: Escaloes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Escalao escalao = db.Escaloes.Find(id);
            db.Escaloes.Remove(escalao);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
