﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjetoEmpresaDasAguasAspNet.Models;

namespace ProjetoEmpresaDasAguasAspNet.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private ProjetoEmpresaDasAguasAspNetContext db = new ProjetoEmpresaDasAguasAspNetContext();
        private ApplicationDbContext dba = new ApplicationDbContext();

        // GET: Dashboard
        public ActionResult Index()
        {
            Dashboard dadosDashboard = new Dashboard();

            #region SUM FATURAS/MÊS

            dadosDashboard.Faturas = new List<Fatura>();

            // Lista de faturas e consumo tendo como base o utilizador
            List<Fatura> listaFaturas = new List<Fatura>();
            List<Consumo> listaConsumos = new List<Consumo>();

            var userId = User.Identity.GetUserId();

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dba));
            var user = userManager.Users.ToList().Find(u => u.Id == userId);

            // Se Admin, se não é cliente
            if (user.TipoConta == "Administrador")
            {
                listaFaturas = db.Faturas.ToList();
                listaConsumos = db.Consumos.ToList();
            }
            else
            {
                listaFaturas = (from fatura in db.Faturas
                    join cliente in db.Clientes on fatura.IdCliente equals cliente.IdCliente
                    where cliente.UserId == user.Id
                    select fatura).ToList();

                listaConsumos = (from consumo in db.Consumos
                    join cliente in db.Clientes on consumo.IdCliente equals cliente.IdCliente
                    where cliente.UserId == user.Id
                    select consumo).ToList();
            }
            
            // Agrupar por mês e ano, e apanhar os 12 meses mais recentes
            var listaSumFaturas = (from faturas in listaFaturas
                group faturas by new { date = new DateTime(faturas.Data.Year, faturas.Data.Month, 1) } into g
                select new Fatura
                {
                    Data = g.Key.date,
                    Total = g.Sum(x => x.Total)
                }).OrderByDescending(f => f.Data).Take(12).ToList();

            // Voltar a order para aparecer as datas no gráfico
            dadosDashboard.Faturas = listaSumFaturas.OrderBy(f => f.Data).ToList();

            #endregion

            #region SUM CONSUMOS/MÊS

            dadosDashboard.Consumos = new List<Consumo>();

            // Agrupar por mês e ano, e apanhar os 12 meses mais recentes
            var listaSumConsumos = (from consumos in listaConsumos
                                    group consumos by new { date = new DateTime(consumos.Data.Year, consumos.Data.Month, 1) } into g
                select new Consumo
                {
                    Data = g.Key.date,
                    Contagem = g.Sum(x => x.Contagem)
                }).OrderByDescending(c => c.Data).Take(12).ToList();

            // Voltar a order para aparecer as datas no gráfico
            dadosDashboard.Consumos = listaSumConsumos.OrderBy(c => c.Data).ToList();

            #endregion

            return View(dadosDashboard);
        }
    }
}