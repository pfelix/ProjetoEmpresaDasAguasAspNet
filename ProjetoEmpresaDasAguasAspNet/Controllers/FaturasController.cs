﻿using ProjetoEmpresaDasAguasAspNet.Models;
using ProjetoEmpresaDasAguasAspNet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjetoEmpresaDasAguasAspNet.Helpers;
using System.Net;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ProjetoEmpresaDasAguasAspNet.Controllers
{
    [Authorize(Roles = "Faturas")]
    public class FaturasController : Controller
    {
        private ApplicationDbContext dbApp = new ApplicationDbContext();
        private ProjetoEmpresaDasAguasAspNetContext db = new ProjetoEmpresaDasAguasAspNetContext();

        // GET: Index
        [Authorize(Roles = "Faturas View")]
        public ActionResult Index (int? searchClient)
        {
            // Garantir que a variável de sessão faturaView fica limpa
            Session["faturaView"] = null;

            // Apagar User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);

            List<Fatura> listaFaturas = new List<Fatura>();

            // Mostar lista diferentes
            if (user.TipoConta == "Administrador")
            {
                if (searchClient == null || searchClient == 0)
                {
                    listaFaturas = db.Faturas.ToList();
                }
                else
                {
                    listaFaturas = db.Faturas.Where(c => c.IdCliente == searchClient).ToList();
                }
            }
            else
            {
                // Lista do consumos que o utilizador está associado
                listaFaturas = (from fatura in db.Faturas
                                join cliente in db.Clientes on fatura.IdCliente equals cliente.IdCliente
                                where cliente.UserId == user.Id
                                select fatura).ToList();
            }

            ViewBag.searchClient = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome", searchClient);
            return View(listaFaturas);
        }

        // GET: FaturaDetalhe
        [Authorize(Roles = "Faturas View")]
        public ActionResult Faturadetalhe(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int idFatura = Convert.ToInt32(id);

            var faturaDetalhes = db.FaturaDetalhes.Where(f => f.IdFatura == idFatura).ToList();
            if (faturaDetalhes.Count == 0)
            {
                return HttpNotFound();
            }

            var fatura = db.Faturas.Find(idFatura);
            var cliente = db.Clientes.Find(fatura.IdCliente);

            FaturaView faturaView = new FaturaView();

            faturaView.Cliente = cliente;
            faturaView.FaturaDetalhes = faturaDetalhes;

            return View(faturaView);
        }

        // GET: Create
        [Authorize(Roles = "Faturas Create")]
        public ActionResult Create()
        {
            // Apanha User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);

            // Para verificar se é primeira vez ou não
            var faturaView = Session["faturaView"] as FaturaView;

            if (faturaView == null)
            {
                faturaView = new FaturaView();
                faturaView.Cliente = new Cliente();
                faturaView.FaturaDetalhes = new List<FaturaDetalhe>();

                Session["faturaView"] = faturaView;

                ViewBag.IdCliente = new SelectList(CombosHelper.ListaClientes(user), "IdCliente", "Nome");

                return View(faturaView);
            }

            ViewBag.IdCliente = new SelectList(CombosHelper.ListaClientes(user), "IdCliente", "Nome", faturaView.Cliente.IdCliente);

            return View(faturaView);
        }

        // POST: Create
        [HttpPost]
        public ActionResult Create(FaturaView faturaView)
        {
            // Apanha User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);

            faturaView = Session["faturaView"] as FaturaView;

            // Verificar se cliente foi selecionado
            if (faturaView.Cliente == null)
            {
                ViewBag.ErrorIdCliente = "Tem que selecionar um cliente e adicionar consumos à fatura";

                ViewBag.IdCliente = new SelectList(CombosHelper.ListaClientes(user), "IdCliente", "Nome");

                return View(faturaView);
            }

            if (faturaView.Cliente.IdCliente == 0)
            {
                ViewBag.ErrorIdCliente = "Tem que selecionar um cliente e adicionar consumos à fatura";

                ViewBag.IdCliente = new SelectList(CombosHelper.ListaClientes(user), "IdCliente", "Nome");

                return View(faturaView);
            }

            // Verifica se fatura tem detalhes
            if (faturaView.FaturaDetalhes.Count == 0)
            {
                ViewBag.ErrorDetalhes = "Tem que inserir consumos primeiro";

                ViewBag.IdCliente = new SelectList(CombosHelper.ListaClientes(user), "IdCliente", "Nome", faturaView.Cliente.IdCliente);

                return View(faturaView);
            }

            // Inserir dados na Base de dados
            int idFatura = 0;

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    double total = 0;

                    // Somar dos consumos e o iva

                    foreach (var item in faturaView.FaturaDetalhes)
                    {
                        total += item.SubTotal * ((item.Iva / 100) + 1);
                    };

                    // Gravar a Fatura
                    var fatura = new Fatura
                    {
                        IdCliente = faturaView.Cliente.IdCliente,
                        Data = DateTime.Now,
                        Total = total
                    };

                    db.Faturas.Add(fatura);

                    db.SaveChanges();

                    // Gravar detalhes da fatura
                    idFatura = fatura.IdFatura;

                    // Forçar o erro para testar
                    //int n = 5;
                    //int x = n / 0;

                    foreach (var item in faturaView.FaturaDetalhes)
                    {
                        var faturaDetalhe = new FaturaDetalhe
                        {
                            IdFatura = idFatura,
                            IdConsumo = item.IdConsumo,
                            Contagem = item.Contagem,
                            Iva = item.Iva,
                            SubTotal = item.SubTotal,
                        };

                        db.FaturaDetalhes.Add(faturaDetalhe);
                        db.SaveChanges();

                        // Marcar consumo como faturado
                        Consumo consumo = db.Consumos.Find(item.IdConsumo);
                        consumo.Faturado = true;

                        db.Entry(consumo).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    transaction.Commit();

                    
                }
                catch (Exception e)
                {
                    transaction.Rollback();

                    ViewBag.Mensagem = $"Erro: {e.Message}";

                    ViewBag.IdCliente = new SelectList(CombosHelper.ListaClientes(user), "IdCliente", "Nome");

                    faturaView = new FaturaView();
                    faturaView.Cliente = new Cliente();
                    faturaView.FaturaDetalhes = new List<FaturaDetalhe>();

                    // Limpar a variável de Sessão
                    Session["faturaView"] = faturaView;

                    return View(faturaView);
                }
                
            }

            // Mostrar caso fatura seja criada com sucesso

            // Limpar variável de Session["faturaView"]
            Session["faturaView"] = null;

            ViewBag.Mensagem = $"Fatura {idFatura} criada com sucesso";

            ViewBag.searchClient = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome");
            return View("Index", db.Faturas.ToList());
        }

        // GET: AdicionarConsumo
        [Authorize(Roles = "Faturas Create")]
        [Route("FaturasController/AdicionarConsumo/{idCliente}")]
        public ActionResult AdicionarConsumo(int? idCliente)
        {
            // Apanha User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);

            var faturaView = Session["faturaView"] as FaturaView;

            // Verifica se não foi passado nenhum numero de cliente
            if (idCliente == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Cliente cliente = db.Clientes.Find(idCliente);
            // Verificar se cliente foi selecionado e se existe
            if (cliente == null)
            {
                ViewBag.ErrorIdCliente = "Tem que selecionar um cliente";

                ViewBag.IdCliente = new SelectList(CombosHelper.ListaClientes(user), "IdCliente", "Nome");

                return View("Create", faturaView);
            }

            faturaView.Cliente = cliente;

            ViewBag.IdConsumo = new SelectList(CombosHelper.ListaConsumosAFaturar((int)idCliente, faturaView.FaturaDetalhes), "IdConsumo", "Data");
                        
            return View();
        }

        // POST: AdicionarConsumo
        [HttpPost]
        public ActionResult AdicionarConsumo(FaturaDetalhe faturaDetalhe)
        {
            // Apanha User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);

            // Receber dados da View
            var faturaView = Session["faturaView"] as FaturaView;

            int idConsumo = 0;

            // Verificar se foi selecionado uma data
            try
            {
                idConsumo = int.Parse(Request["IdConsumo"]);
            }
            catch
            {
                ViewBag.ErrorData = "Tem que selecionar uma data";

                ViewBag.IdConsumo = new SelectList(CombosHelper.ListaConsumosAFaturar((int)faturaView.Cliente.IdCliente, faturaView.FaturaDetalhes), "IdConsumo", "Data");

                return View();
            }
            
            // Verifica se consumo existe
            var consumo = db.Consumos.Find(idConsumo);
            if (consumo == null)
            {
                ViewBag.Mensagem = "O consumo selecionado não existe.";

                ViewBag.IdConsumo = new SelectList(CombosHelper.ListaConsumosAFaturar((int)faturaView.Cliente.IdCliente, faturaView.FaturaDetalhes), "IdConsumo", "Data");

                return View();
            }

            // Calcular o valor do consumo
            double subTotal = CalcularValorSubTotal(consumo.Contagem);

            // Criar o detalhe da fatura
            faturaDetalhe = new FaturaDetalhe
            {
                IdConsumo = consumo.IdConsumo,
                Contagem = consumo.Contagem,
                SubTotal = subTotal,
                Iva = faturaDetalhe.Iva
            };

            // Adicionar o detalhe à lista FaturaDetalhes que está na faturaView
            faturaView.FaturaDetalhes.Add(faturaDetalhe);

            // Criar lista para DropBoxList com o IdCliente já selecioando
            ViewBag.IdCliente = new SelectList(CombosHelper.ListaClientes(user), "IdCliente", "Nome", faturaView.Cliente.IdCliente);

            return View("Create", faturaView);
        }

        // GET: DeleteFaturaDetalhe
        [Authorize(Roles = "Faturas Create")]
        public ActionResult DeleteFaturaDetalhe(int? idxFaturaDetalhe)
        {
            if (idxFaturaDetalhe == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int idxFaturaDetalheConvert = Convert.ToInt32(idxFaturaDetalhe);

            var faturaView = Session["faturaView"] as FaturaView;

            // Apanha User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);

            if (faturaView.Cliente == null)
            {
                ViewBag.ErrorIdCliente = "Tem que selecionar um cliente e adicionar consumos";

                faturaView = new FaturaView();
                faturaView.Cliente = new Cliente();
                faturaView.FaturaDetalhes = new List<FaturaDetalhe>();

                Session["faturaView"] = faturaView;

                ViewBag.IdCliente = new SelectList(CombosHelper.ListaClientes(user), "IdCliente", "Nome");

                return View("Create", faturaView);
            };

            // Procurar na lista se existe o idxFaturaDetalhe
            if (idxFaturaDetalheConvert > -1 && faturaView.FaturaDetalhes.Count >= idxFaturaDetalheConvert + 1)
            {
                FaturaDetalhe faturaDetalhe = faturaView.FaturaDetalhes[idxFaturaDetalheConvert];

                return View(faturaDetalhe);
            }

            // Sem nenhum dado para apagar returnar ao Index
            Session["faturaView"] = null;

            ViewBag.searchClient = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome");
            return View("Index", db.Faturas.ToList());
        }

        // POST: DeleteFaturaDetalhe
        [HttpPost]
        public ActionResult DeleteFaturaDetalhe(int idxFaturaDetalhe)
        {
            // Apanha User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);

            var faturaView = Session["faturaView"] as FaturaView;

            faturaView.FaturaDetalhes.RemoveAt(idxFaturaDetalhe);

            ViewBag.IdCliente = new SelectList(CombosHelper.ListaClientes(user), "IdCliente", "Nome", faturaView.Cliente.IdCliente);

            return View("Create", faturaView);
        }



        /// <summary>
        /// Metodo para calcular o subtotal da fatura
        /// </summary>
        /// <param name="contagem"></param>
        /// <returns></returns>
        private double CalcularValorSubTotal(int contagem)
        {
            // Obter a lista de todos os escaloes onde entra a contagem

            List<Escalao> listaEscaloes = (
                from escaloes
                in db.Escaloes
                where contagem > escaloes.Minimo
                orderby escaloes.Minimo
                select escaloes).ToList();

            double subTotal = 0;
            int jaEmitido = 0;

            // ciclo para calcular o subtotal
            for (int i = 0; i < listaEscaloes.Count; i++)
            {
                // se for o ultimo idx da lista
                if (i == listaEscaloes.Count - 1)
                {
                    subTotal += listaEscaloes[i].ValorUnitario * contagem;
                }
                else
                {
                    // Calculo do subtotal:
                    // 1 - Vai buscar o valorUnitario do idx
                    // 2 - Vai buscar o valor minimo do escalao a seguir e retira 1
                    // 3 - Ao ponto 2 retiramos a contagem que já foi contabilizada
                    // 4 - Multiplica-se o ponto 1 com o ponto 3
                    subTotal += listaEscaloes[i].ValorUnitario * (listaEscaloes[i + 1].Minimo - 1 - jaEmitido);
                    contagem -= listaEscaloes[i + 1].Minimo - 1 - jaEmitido;
                    jaEmitido = listaEscaloes[i + 1].Minimo - 1;
                }
            }

            return subTotal;
        }
    }
}