﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjetoEmpresaDasAguasAspNet.Models;

namespace ProjetoEmpresaDasAguasAspNet.Controllers
{
    [Authorize(Roles = "Consumos")]
    public class ConsumosController : Controller
    {
        private ApplicationDbContext dbApp = new ApplicationDbContext();
        private ProjetoEmpresaDasAguasAspNetContext db = new ProjetoEmpresaDasAguasAspNetContext();

        // GET: Consumos
        [Authorize(Roles = "Consumos View")]
        public ActionResult Index(int? searchClient)
        {
            // Apagar User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);

            List<Consumo> listaConsumos = new List<Consumo>();

            if (user.TipoConta == "Administrador")
            {
                if (searchClient == null || searchClient == 0)
                {
                    listaConsumos = db.Consumos.ToList();
                }
                else
                {
                    listaConsumos = db.Consumos.Where(c => c.IdCliente == searchClient).ToList();
                }
            }
            else
            {
                // Lista do consumos que o utilizador está associado
                listaConsumos = (from consumo in db.Consumos
                                join cliente in db.Clientes on consumo.IdCliente equals cliente.IdCliente
                                where cliente.UserId == user.Id
                                select consumo).ToList();
            }

            ViewBag.searchClient = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome", searchClient);
            return View(listaConsumos.OrderBy(c => c.Data));
        }

        // GET: Consumos/Create
        [Authorize(Roles = "Consumos Create")]
        public ActionResult Create()
        {
            // Apanhar User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);
            
            ViewBag.IdCliente = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome");
            return View();
        }

        // POST: Consumos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdConsumo,IdCliente,Data,Contagem")] Consumo consumo)
        {
            // Apanhar User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);

            // Verificar se cliente foi selecionado
            if (consumo.IdCliente == 0)
            {
                ViewBag.Error = "Tem que selecionar um cliente";
                ViewBag.IdCliente = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome", consumo.IdCliente);
                return View(consumo);
            }

            // Verifica se ja foi inserido um consumo neste mês, para este cliente
            if (VerificarDataMes(consumo, "Create"))
            {
                ViewBag.Error = "Já inseriu uma contagem para este mês!";
                ViewBag.IdCliente = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome", consumo.IdCliente);
                return View(consumo);
            }

            // Verifica se está a inserir um registo anterior ao ultimo registo
            if (VerificarUltimaData(consumo, "Create"))
            {
                ViewBag.Error = "Não pode inserir ou actualizar uma contagem\ncom uma data anterior às contagens inseridas";
                ViewBag.IdCliente = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome", consumo.IdCliente);
                return View(consumo);
            }

            // Verifica o Model
            if (ModelState.IsValid)
            {
                db.Consumos.Add(consumo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCliente = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome", consumo.IdCliente);
            return View(consumo);
        }
        
        // GET: Consumos/Edit/5
        [Authorize(Roles = "Consumos Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consumo consumo = db.Consumos.Find(id);
            if (consumo == null)
            {
                return HttpNotFound();
            }

            if (consumo.Faturado)
            {
                // Parecido com a ViewBag, mas precesa os dados num RedirectToAction e até ser chamado para leirura
                TempData["Error"] = "Não pode alterar um consumo já faturado";

                return RedirectToAction("Index");

                // Outra opção atráves da View
                // var consumoes = db.Consumos.Include(c => c.Cliente);
                // return View("Index", consumoes);
            }

            // Apanhar User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);

            ViewBag.IdCliente = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome", consumo.IdCliente);
            return View(consumo);
        }

        // POST: Consumos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdConsumo,IdCliente,Data,Contagem")] Consumo consumo)
        {
            // Apanhar User logado
            string userId = User.Identity.GetUserId();

            // Procurar 
            var managerUser = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbApp));
            var user = managerUser.Users.ToList().Find(u => u.Id == userId);

            // Verifica se ja foi inserido um consumo neste mês, para este cliente
            if (VerificarDataMes(consumo, "Edit"))
            {
                ViewBag.Error = "Já inseriu uma contagem para este mês!";
                ViewBag.IdCliente = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome", consumo.IdCliente);
                return View(consumo);
            }

            // Verifica se está a inserir um registo anterior ao ultimo registo
            if (VerificarUltimaData(consumo, "Edit"))
            {
                ViewBag.Error = "Não pode inserir ou actualizar uma contagem\ncom uma data anterior às contagens inseridas";
                ViewBag.IdCliente = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome", consumo.IdCliente);
                return View(consumo);
            }

            // Verifica o Model
            if (ModelState.IsValid)
            {
                db.Entry(consumo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCliente = new SelectList(Helpers.CombosHelper.ListaClientes(user), "IdCliente", "Nome", consumo.IdCliente);
            return View(consumo);
        }

        // GET: Consumos/Delete/5
        [Authorize(Roles = "Consumos Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consumo consumo = db.Consumos.Find(id);
            if (consumo == null)
            {
                return HttpNotFound();
            }

            if (consumo.Faturado)
            {
                // Parecido com a ViewBag, mas precesa os dados num RedirectToAction e até ser chamado para leirura
                TempData["Error"] = "Não pode apagar um consumo já faturado";

                return RedirectToAction("Index");
            }

            return View(consumo);
        }

        // POST: Consumos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Consumo consumo = db.Consumos.Find(id);
            db.Consumos.Remove(consumo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Verifica se existe algum consumo já registado no mês e no cliente
        /// </summary>
        /// <param name="consumo"></param>
        /// <param name="tipoEdicao"></param>
        /// <returns></returns>
        private bool VerificarDataMes(Consumo consumo, string tipoEdicao)
        {
            // Lista de todos os consumos
            var listaConsumo = db.Consumos.AsNoTracking().ToList();

            // Lista de consumos do mês
            List<Consumo> verificarDataMes = listaConsumo
                .Where(c =>
                c.Data.Year == consumo.Data.Year
                &&
                c.Data.Month == consumo.Data.Month
                &&
                c.IdCliente == consumo.IdCliente).ToList();

            // Se for pelo Create
            if (tipoEdicao == "Create")
            {
                // Se existir registos
                if (verificarDataMes.Count > 0)
                {
                    return true;
                }
            }

            // Se for pelo Edit
            if (tipoEdicao == "Edit")
            {
                // Se existir registos
                if (verificarDataMes.Count > 0)
                {
                    // Se o IdConsumo mais resente for diferente do IdConsumo que está a ser atualizado
                    if (verificarDataMes[0].Data.Year == consumo.Data.Year 
                        && verificarDataMes[0].Data.Month == consumo.Data.Month 
                        && verificarDataMes[0].IdConsumo != consumo.IdConsumo)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Verifica se a data de registo do consumo é anterior à ultima data
        /// </summary>
        /// <param name="consumo"></param>
        /// <param name="tipoEdicao"></param>
        /// <returns></returns>
        private bool VerificarUltimaData(Consumo consumo, string tipoEdicao)
        {
            // Lista de todos os consumos
            var listaConsumo = db.Consumos.AsNoTracking().ToList();

            // Procurar consumo mais recente do cliente
            var verificarUltimaData = listaConsumo
                .Where(c => c.IdCliente == consumo.IdCliente)
                .OrderByDescending(c => c.Data)
                .FirstOrDefault();

            // Se não existe algum registo
            if (verificarUltimaData == null)
            {
                return false;
            }

            // Verificar se a nova data é inferior à ultima data registada na base de dados do cliente
            if (verificarUltimaData.Data > consumo.Data)
            {
                // Verifica se a ultima data registada é o mesmo registo que está a ser alterado
                if (verificarUltimaData.IdConsumo == consumo.IdConsumo)
                {
                    // Procurar consumos do cliente
                    List<Consumo> listaSemRegistoMaisRecente = listaConsumo
                        .Where(c => c.IdCliente == consumo.IdCliente)
                        .OrderByDescending(c => c.Data).ToList();

                    // Remove o consumo mais recente
                    listaSemRegistoMaisRecente.Remove(verificarUltimaData);

                    // Se não existe algum registo
                    if (listaSemRegistoMaisRecente.Count == 0)
                    {
                        return false;
                    };

                    // Verificar se a nova data é inferior à ultima data registada na base de dados do cliente
                    if (listaSemRegistoMaisRecente[0].Data > consumo.Data)
                    {
                        return true;
                    }

                    return false;
                };

                return true;
            }

            return false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
