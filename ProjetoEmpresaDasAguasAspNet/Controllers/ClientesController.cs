﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjetoEmpresaDasAguasAspNet.Helpers;

namespace ProjetoEmpresaDasAguasAspNet.Controllers
{
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;
    using ProjetoEmpresaDasAguasAspNet.Models;

    [Authorize(Roles = "Clientes")]
    public class ClientesController : Controller
    {
        private ApplicationDbContext dbUser = new ApplicationDbContext();
        private ProjetoEmpresaDasAguasAspNetContext db = new ProjetoEmpresaDasAguasAspNetContext();

        // GET: Clientes
        [Authorize(Roles = "Clientes View")]
        public ActionResult Index()
        {
            var clientes = db.Clientes.ToList();

            // Para mostrar o nome do utilizador em vez do UserId
            var listaUsers = CombosHelper.ListaUsers();

            foreach (var cliente in clientes)
            {
                if (cliente.UserId == null)
                {
                    cliente.UserId = "Sem utilizador";
                }
                else
                {
                    cliente.UserId = listaUsers.FirstOrDefault(u => u.Id == cliente.UserId).UserName;
                }
            }

            return View(db.Clientes.ToList());
        }

        // GET: Clientes/Details/5
        [Authorize(Roles = "Clientes View")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }

            // Para mostrar o nome do utilizador em vez do UserId
            var listaUsers = CombosHelper.ListaUsers();
            var user = listaUsers.FirstOrDefault(u => u.Id == cliente.UserId);

            if (user == null)
            {
                cliente.UserId = "Sem utilizador";
            }
            else
            {
                cliente.UserId = user.UserName;
            }

            return View(cliente);
        }

        // GET: Clientes/Create
        [Authorize(Roles = "Clientes Create")]
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(CombosHelper.ListaUsers(), "Id", "UserName");
            return View();
        }

        // POST: Clientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdCliente,Nome,Morada,CodPostal,Nif,UserId")] Cliente cliente)
        {
            // Verificar se NIF já existe
            if (NifDuplicado(cliente, "Create"))
            {
                ViewBag.Error = "Já existe um cliente com esse NIF";

                ViewBag.UserId = new SelectList(CombosHelper.ListaUsers(), "Id", "UserName", cliente.UserId);
                return View(cliente);
            }

            // Verificar se foi selecionado uma opçao no utilizar. [Selecione um utilizador...] - Nenhuma opção selecionada
            if (cliente.UserId == "[Selecione um utilizador...]")
            {
                ViewBag.ErrorUserId = "Tem que escolher um utilizar ou selecionar a opção Sem utilizador.";

                ViewBag.UserId = new SelectList(CombosHelper.ListaUsers(), "Id", "UserName");
                return View(cliente);
            }

            if (ModelState.IsValid)
            {
                db.Clientes.Add(cliente);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(CombosHelper.ListaUsers(), "Id", "UserName", cliente.UserId);
            return View(cliente);
        }

        // GET: Clientes/Edit/5
        [Authorize(Roles = "Clientes Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserId = new SelectList(CombosHelper.ListaUsers(), "Id", "UserName", cliente.UserId);
            return View(cliente);
        }

        // POST: Clientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdCliente,Nome,Morada,CodPostal,Nif,UserId")] Cliente cliente)
        {
            // Verificar se NIF já existe
            if (NifDuplicado(cliente, "Edit"))
            {
                ViewBag.Error = "Já existe um cliente com esse NIF";

                ViewBag.UserId = new SelectList(CombosHelper.ListaUsers(), "Id", "UserName", cliente.UserId);
                return View(cliente);
            }

            // Verificar se foi selecionado uma opçao no utilizar. [Selecione um utilizador...] - Nenhuma opção selecionada
            if (cliente.UserId == "[Selecione um utilizador...]")
            {
                ViewBag.ErrorUserId = "Tem que escolher um utilizar ou selecionar a opção Sem utilizador.";

                ViewBag.UserId = new SelectList(CombosHelper.ListaUsers(), "Id", "UserName");
                return View(cliente);
            }

            if (ModelState.IsValid)
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(CombosHelper.ListaUsers(), "Id", "UserName", cliente.UserId);
            return View(cliente);
        }

        // GET: Clientes/Delete/5
        [Authorize(Roles = "Clientes Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }

            // Verificar se existe algum consumo registado no cliente
            var findConsumo = db.Consumos.FirstOrDefault(c => c.IdCliente == id);

            if (findConsumo != null)
            {
                var clientes = db.Clientes.ToList();

                // Para mostrar o nome do utilizador em vez do UserId
                var listaUsers = CombosHelper.ListaUsers();

                foreach (var clt in clientes)
                {
                    if (clt.UserId == null)
                    {
                        clt.UserId = "Sem utilizador";
                    }
                    else
                    {
                        clt.UserId = listaUsers.FirstOrDefault(u => u.Id == clt.UserId).UserName;
                    }
                }

                ViewBag.Error = "O Cliente não pode ser apagado porque existem consumos registados.";

                return View("Index",db.Clientes.ToList());
            }

            // Verificar se existe alguma fatura registada no cliente
            var findFatura = db.Faturas.FirstOrDefault(c => c.IdCliente == id);

            if (findFatura != null)
            {
                var clientes = db.Clientes.ToList();

                // Para mostrar o nome do utilizador em vez do UserId
                var listaUsers = CombosHelper.ListaUsers();

                foreach (var clt in clientes)
                {
                    if (clt.UserId == null)
                    {
                        clt.UserId = "Sem utilizador";
                    }
                    else
                    {
                        clt.UserId = listaUsers.FirstOrDefault(u => u.Id == clt.UserId).UserName;
                    }
                }

                ViewBag.Error = "O Cliente não pode ser apagado porque existem faturas registadas.";

                return View("Index", db.Clientes.ToList());
            }

            return View(cliente);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cliente cliente = db.Clientes.Find(id);
            db.Clientes.Remove(cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Verifcar se o NIF é duplicado tendo como base se é create ou edit
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="tipoEdicao"></param>
        /// <returns></returns>
        private bool NifDuplicado (Cliente cliente, string tipoEdicao)
        {
            // Verificar se NIF já existe
            //var procurarNif = db.Clientes.AsNoTracking().FirstOrDefault(c => c.Nif == cliente.Nif);
            var procurarNif = db.Clientes.AsNoTracking().FirstOrDefault(c => c.Nif == cliente.Nif);

            if (procurarNif == null)
            {
                return false;
            }

            // Se for pelo Edit
            if(tipoEdicao == "Edit")
            {

                // Verificar se o NIF encontrato é do cliente que está a ser atualizado os dados
                if (cliente.IdCliente == procurarNif.IdCliente)
                {
                    return false;
                }
            }

            return true;

        }


    }
}
