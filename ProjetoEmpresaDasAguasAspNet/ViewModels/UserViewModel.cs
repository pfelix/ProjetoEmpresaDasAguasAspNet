﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjetoEmpresaDasAguasAspNet.ViewModels
{
    public class UserViewModel
    {
        public string UserId { get; set; }

        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Tipo de Conta")]
        public string TipoConta { get; set; }
    }
}