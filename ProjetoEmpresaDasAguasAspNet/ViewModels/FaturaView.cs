﻿using ProjetoEmpresaDasAguasAspNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoEmpresaDasAguasAspNet.ViewModels
{
    public class FaturaView
    {
        public Cliente Cliente { get; set; }

        public FaturaDetalhe FaturaDetalhe { get; set; }

        public List<FaturaDetalhe> FaturaDetalhes { get; set; }
    }
}