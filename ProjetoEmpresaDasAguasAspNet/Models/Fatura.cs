﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjetoEmpresaDasAguasAspNet.Models
{
    public class Fatura
    {
        [Key]
        [Display(Name = "N. Fatura")]
        public int IdFatura { get; set; }

        [Display(Name = "N. Cliente")]
        public int IdCliente { get; set; }

        [DataType(DataType.Date)]
        public DateTime Data { get; set; }

        public double Total { get; set; }

        // Ligação Cliente - Faturas: 1 para muitos
        public virtual Cliente Cliente { get; set; }

        // Ligação Fatura - FaturaDetalhe: 1 para muitos
        public virtual List<FaturaDetalhe> FaturaDetalhes { get; set; }

    }
}