﻿namespace ProjetoEmpresaDasAguasAspNet.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Cliente
    {
        [Key]
        [Display(Name = "N. Cliente")]
        public int IdCliente { get; set; }

        [Required(ErrorMessage = "Tem que inserir um {0}")]
        public string Nome { get; set; }

        public string Morada { get; set; }

        [Display(Name = "Cod. Postal")]
        public string CodPostal { get; set; }

        [Required(ErrorMessage = "Tem que inserir um {0}")]
        public int Nif { get; set; }

        [Display(Name = "User")]
        public string UserId { get; set; }

        // Ligação Cliente - Consumo: 1 para muitos
        public virtual ICollection<Consumo> Consumos { get; set; }

    }
}