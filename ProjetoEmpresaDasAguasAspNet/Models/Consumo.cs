﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjetoEmpresaDasAguasAspNet.Models
{
    public class Consumo
    {
        [Key]
        [Display(Name = "N. Consumo")]
        public int IdConsumo { get; set; }

        [Display(Name = "Cliente")]
        [Required(ErrorMessage = "Tem que inserir um {0}")]
        public int IdCliente { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Tem que inserir uma {0}")]
        public DateTime Data { get; set; }

        [Required(ErrorMessage = "Tem que inserir uma {0}")]
        [Range(0, Int32.MaxValue, ErrorMessage = "A {0} que que ser enter {1} e {2}")]
        [Display(Name = "Contagem")]
        public int Contagem { get; set; }

        public bool Faturado { get; set; }

        // Ligação Cliente - Consumo: 1 para muitos
        public virtual Cliente Cliente { get; set; }

        // Ligação Consumo - FaturaDetalhe: 1 para muitos
        public virtual List<FaturaDetalhe> FaturaDetalhes { get; set; }

        // Pré-definir Faturado como falso
        public Consumo()
        {
            Faturado = false;
        }

    }
}