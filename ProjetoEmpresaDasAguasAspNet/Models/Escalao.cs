﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjetoEmpresaDasAguasAspNet.Models
{
    public class Escalao
    {
        [Key]
        public int IdEscalao { get; set; }

        [Required(ErrorMessage = "Tem que inserir um valor {0}")]
        public int Minimo { get; set; }

        [Display(Name = "Valor Unitário")]
        [Required(ErrorMessage = "Tem que inserir um {0}")]
        public double ValorUnitario { get; set; }

    }
}