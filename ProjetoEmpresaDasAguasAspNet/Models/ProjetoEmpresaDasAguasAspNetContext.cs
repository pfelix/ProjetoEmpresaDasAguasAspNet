﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ProjetoEmpresaDasAguasAspNet.Models
{
    public class ProjetoEmpresaDasAguasAspNetContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public ProjetoEmpresaDasAguasAspNetContext() : base("name=ProjetoEmpresaDasAguasAspNetContext")
        {
        }

        // Para retirar a predefinição de Cascade on delete
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public System.Data.Entity.DbSet<ProjetoEmpresaDasAguasAspNet.Models.Cliente> Clientes { get; set; }

        public System.Data.Entity.DbSet<ProjetoEmpresaDasAguasAspNet.Models.Consumo> Consumos { get; set; }

        public System.Data.Entity.DbSet<ProjetoEmpresaDasAguasAspNet.Models.Escalao> Escaloes { get; set; }

        public System.Data.Entity.DbSet<ProjetoEmpresaDasAguasAspNet.Models.Fatura> Faturas { get; set; }

        public System.Data.Entity.DbSet<ProjetoEmpresaDasAguasAspNet.Models.FaturaDetalhe> FaturaDetalhes { get; set; }

    }
}
