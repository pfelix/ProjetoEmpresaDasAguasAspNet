﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjetoEmpresaDasAguasAspNet.Controllers;

namespace ProjetoEmpresaDasAguasAspNet.Models
{
    public class Dashboard
    {

        public List<Consumo> Consumos { get; set; }

        public List<Fatura> Faturas { get; set; }

    }
}