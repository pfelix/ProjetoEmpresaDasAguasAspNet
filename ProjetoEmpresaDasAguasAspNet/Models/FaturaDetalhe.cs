﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjetoEmpresaDasAguasAspNet.Models
{
    public class FaturaDetalhe
    {
        [Key]
        [Display(Name = "N. Detalhe de Fatura")]
        public int IdFaturaDetalhe { get; set; }

        [Display(Name = "N. Fatura")]
        public int IdFatura { get; set; }

        [Display(Name = "N. Consumo")]
        public int IdConsumo { get; set; }

        public int Contagem { get; set; }

        // public double ValorUnitario { get; set; }

        [Display(Name = "IVA (%)")]
        public double Iva { get; set; }

        public double SubTotal { get; set; }

        // Ligação Consumo - FaturaDetalhe: 1 para muitos
        public virtual Consumo Consumo { get; set; }

        // Ligação Fatura - FaturaDetalhe: 1 para muitos
        public virtual Fatura Fatura { get; set; }

    }
}