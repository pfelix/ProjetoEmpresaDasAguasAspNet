﻿using ProjetoEmpresaDasAguasAspNet.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ProjetoEmpresaDasAguasAspNet.Helpers
{
    public class CombosHelper
    {
        private static ApplicationDbContext dbUsers = new ApplicationDbContext();

        private static ProjetoEmpresaDasAguasAspNetContext db = new ProjetoEmpresaDasAguasAspNetContext();

        /// <summary>
        /// Retorna lista de todos os cliente se administrador
        /// Retorna lista de clientes associados se não for administrador
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<Cliente> ListaClientes(ApplicationUser user)
        {

            List<Cliente> clientes = new List<Cliente>();

            if (user.TipoConta == "Administrador")
            {
                clientes = db.Clientes.ToList();
                clientes.Add(new Cliente
                {
                    IdCliente = 0,
                    Nome = "[Selecione um cliente...]"
                });
            }
            else
            {
                clientes = db.Clientes.Where(u => u.UserId == user.Id).ToList();
                clientes.Add(new Cliente
                {
                    IdCliente = 0,
                    Nome = "[Selecione um cliente...]"
                });
            }

            return clientes.OrderBy(c => c.Nome).ToList();
        }

        /// <summary>
        /// retrona lista de consumos que podem ser faturados
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="faturaDetalhes"></param>
        /// <returns></returns>
        public static List<Consumo> ListaConsumosAFaturar(int idCliente, List<FaturaDetalhe> faturaDetalhes)
        {
            var consumos = db.Consumos.Where(c => c.IdCliente == idCliente && c.Faturado == false).ToList();

            // Criar nova lista com consumos a faturar
            List<Consumo> consumosAFaturar = new List<Consumo>(consumos);

            // Verifica se existem detalhes já pré-registados e remover da lista consumosAFaturar
            if (faturaDetalhes.Count > 0)
            {
                foreach (var faturaDetalhe in faturaDetalhes)
                {
                    foreach (var consumo in consumos)
                    {
                        if (consumo.IdConsumo == faturaDetalhe.IdConsumo)
                        {
                            consumosAFaturar.Remove(consumo);
                        }
                    }
                }
            }

            return consumosAFaturar.OrderBy(c => c.Data).ToList();
        }

        /// <summary>
        /// Retorna lista de todos os utilizadores menos os administradores
        /// Campo não válido [Selecione um utilizador...]
        /// </summary>
        /// <returns></returns>
        public static List<ApplicationUser> ListaUsers()
        {
            var Users = dbUsers.Users.Where(u => u.TipoConta != "Administrador").ToList();

            Users.Add(new ApplicationUser
            {
                Id = "[Selecione um utilizador...]",
                UserName = "[Selecione um utilizador...]"
            });

            // Atenção que este ID está a ser guardado na BD
            Users.Add(new ApplicationUser
            {
                Id = "Sem utilizador",
                UserName = "Sem utilizador"
            });

            return Users.OrderBy(c => c.UserName).ToList();
        }

        /// <summary>
        /// Retorna uma lista com os tipos de conta
        /// Campo não válido é [Selecione um tipo de conta...]
        /// </summary>
        /// <returns></returns>
        public static List<string> ListaTipoConta()
        {
            List<string> tipoContas = new List<string>();

            tipoContas = Enum.GetValues(typeof(TipoConta))
                        .Cast<TipoConta>()
                        .Select(v => v.ToString())
                        .ToList();

            tipoContas.Add("[Selecione um tipo de conta...]");

            tipoContas.Sort();

            return tipoContas;
        }
    }
}