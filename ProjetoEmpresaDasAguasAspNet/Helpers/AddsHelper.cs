﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjetoEmpresaDasAguasAspNet.Models;

namespace ProjetoEmpresaDasAguasAspNet.Helpers
{
    public class AddsHelper
    {
        /// <summary>
        /// Metodo para Adicionar Roles ao utilizar quando for associado a um cliente
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="db"></param>
        public static void AdicionarRoles(string userId, UserManager<ApplicationUser> userManager)
        {

            //var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userId);

            // Não existe utilizador
            if (user == null)
            {
                return;
            }

            // Permissões para Clientes
            if (user.TipoConta == "Cliente")
            {
                if (!userManager.IsInRole(user.Id, "Consumos"))
                {
                    userManager.AddToRole(user.Id, "Consumos");
                }

                if (!userManager.IsInRole(user.Id, "Consumos View"))
                {
                    userManager.AddToRole(user.Id, "Consumos View");
                }

                if (!userManager.IsInRole(user.Id, "Faturas"))
                {
                    userManager.AddToRole(user.Id, "Faturas");
                }

                if (!userManager.IsInRole(user.Id, "Faturas View"))
                {
                    userManager.AddToRole(user.Id, "Faturas View");
                }

                if (!userManager.IsInRole(user.Id, "Consumos Create"))
                {
                    userManager.AddToRole(user.Id, "Consumos Create");
                }

                return;
            }

            // Permissões para Administradores
            if (user.TipoConta == "Administrador")
            {
                // Verificar se já tem as roles e adicionar
                // Roles Admin
                if (!userManager.IsInRole(user.Id, "Admin"))
                {
                    userManager.AddToRole(user.Id, "Admin");
                }

                // Roles Clientes
                if (!userManager.IsInRole(user.Id, "Clientes"))
                {
                    userManager.AddToRole(user.Id, "Clientes");
                }

                if (!userManager.IsInRole(user.Id, "Clientes View"))
                {
                    userManager.AddToRole(user.Id, "Clientes View");
                }

                if (!userManager.IsInRole(user.Id, "Clientes Create"))
                {
                    userManager.AddToRole(user.Id, "Clientes Create");
                }

                if (!userManager.IsInRole(user.Id, "Clientes Edit"))
                {
                    userManager.AddToRole(user.Id, "Clientes Edit");
                }

                if (!userManager.IsInRole(user.Id, "Clientes Delete"))
                {
                    userManager.AddToRole(user.Id, "Clientes Delete");
                }

                // Roles Consumos
                if (!userManager.IsInRole(user.Id, "Consumos"))
                {
                    userManager.AddToRole(user.Id, "Consumos");
                }

                if (!userManager.IsInRole(user.Id, "Consumos View"))
                {
                    userManager.AddToRole(user.Id, "Consumos View");
                }

                if (!userManager.IsInRole(user.Id, "Consumos Create"))
                {
                    userManager.AddToRole(user.Id, "Consumos Create");
                }

                if (!userManager.IsInRole(user.Id, "Consumos Edit"))
                {
                    userManager.AddToRole(user.Id, "Consumos Edit");
                }

                if (!userManager.IsInRole(user.Id, "Consumos Delete"))
                {
                    userManager.AddToRole(user.Id, "Consumos Delete");
                }

                // Roles Escaloes
                if (!userManager.IsInRole(user.Id, "Escaloes"))
                {
                    userManager.AddToRole(user.Id, "Escaloes");
                }

                if (!userManager.IsInRole(user.Id, "Escaloes View"))
                {
                    userManager.AddToRole(user.Id, "Escaloes View");
                }

                if (!userManager.IsInRole(user.Id, "Escaloes Create"))
                {
                    userManager.AddToRole(user.Id, "Escaloes Create");
                }

                if (!userManager.IsInRole(user.Id, "Escaloes Edit"))
                {
                    userManager.AddToRole(user.Id, "Escaloes Edit");
                }

                if (!userManager.IsInRole(user.Id, "Escaloes Delete"))
                {
                    userManager.AddToRole(user.Id, "Escaloes Delete");
                }

                // Roles Faturas
                if (!userManager.IsInRole(user.Id, "Faturas"))
                {
                    userManager.AddToRole(user.Id, "Faturas");
                }

                if (!userManager.IsInRole(user.Id, "Faturas View"))
                {
                    userManager.AddToRole(user.Id, "Faturas View");
                }

                if (!userManager.IsInRole(user.Id, "Faturas Create"))
                {
                    userManager.AddToRole(user.Id, "Faturas Create");
                }

                // Role Utilizadores
                if (!userManager.IsInRole(user.Id, "Users"))
                {
                    userManager.AddToRole(user.Id, "Users");
                }

                if (!userManager.IsInRole(user.Id, "Users View"))
                {
                    userManager.AddToRole(user.Id, "Users View");
                }

                if (!userManager.IsInRole(user.Id, "Users Create"))
                {
                    userManager.AddToRole(user.Id, "Users Create");
                }

                if (!userManager.IsInRole(user.Id, "Users Edit"))
                {
                    userManager.AddToRole(user.Id, "Users Edit");
                }

                if (!userManager.IsInRole(user.Id, "Users Delete"))
                {
                    userManager.AddToRole(user.Id, "Users Delete");
                }
            }
        }
    }
}